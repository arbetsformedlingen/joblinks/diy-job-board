# DIY job board in 5 minutes

Build your own job-board at no cost. Serverless webapp serving 50 000 ads running only in your browser.

1. KISS - Solution based on a few static files
2. License - Ads are open data and this project is open source
3. Cost - Free hosting on Gitlab
4. Performance - Search time around 10-200 msek

# LIVE DEMO (data is updated every day)
https://arbetsformedlingen.gitlab.io/joblinks/diy-job-board/

# Update procedure
https://gitlab.com/arbetsformedlingen/joblinks/diy-job-board/-/pipeline_schedules

# Data source
https://data.jobtechdev.se/annonser/jobtechlinks/

# RUN on localhost

Clone the repository
```
git clone https://gitlab.com/arbetsformedlingen/joblinks/diy-job-board
```
RUN
```
cd diy-job-board &&
docker run -it --rm -d -p 8080:80 --name www -v $(pwd)/public:/usr/share/nginx/html nginx
```
HOST
```
Use Gitlab pages to host your own job-board for FREE! The public/ catalogue are hosting this DIY job board (https://arbetsformedlingen.gitlab.io/joblinks/diy-job-board/)
```

# Development

Required installed software are [Bash](https://www.gnu.org/software/bash), [jq](https://stedolan.github.io/jq/), [Curl](https://curl.haxx.se/), [Node](https://nodejs.org/en/) and [Lunrjs](https://lunrjs.com/guides/getting_started.html#installation). Required data are ads_enriched.json that is used to create the search index.  

> TIP! Develop your own module to create a new attribute to search on.

### DATA

This project uses data/ads_enriched.json.gz as source for job ads.

Use data/ads_raw.json.gz as input file to https://gitlab.com/arbetsformedlingen/joblinks/pipeline if you want to create your own module that enriches ads.

### BUILD

*Install Lunrjs*

```sh
npm install lunr
```

*REBUILD INDEX*

index.json is a IDF index.

Attribute titel(title),yrke(occupation) and ort(pace) are indexed. Extract other attributes with the below code and change build/build-index.js to create a better index.

```sh
gunzip -c data/ads_enriched.json.gz \
	| jq -c 'select(.isValid==true)' \
	| jq '{id:.id,title: .originalJobPosting.title,ort: .workplace_addresses[0].municipality,yrke: .text_enrichments_results.enrichedbinary_result.enriched_candidates.occupations[0].concept_label}' \
	| jq -s '.' \
	| node build/build-index.js > data/index.json
```

*BUILD WWW*

links.json - All the ads as html.

```sh
gunzip -c data/ads_enriched.json.gz \
	| jq -c 'select(.isValid==true)' \
	| jq '. |[.id, .originalJobPosting.title,.workplace_addresses[0].municipality, .sourceLinks[0].link,.originalJobPosting.description[0:100], .originalJobPosting.hiringOrganization.name]' \
	| jq '"<li id=\""+(.[0]|tostring)+"\" class=\"mt-0 mb-0\"><div class=\"card-body\"><h6 class=\"card-subtitle mb-2 text-muted\">"+.[3][0:30]+"...</h6><h5><a class=\"card-title text-primary\" href=\""+.[3]+"\">"+.[1]+"</a></h5><p class=\"card-text mb-0\">"+.[4]+"...</p><div class=\"mt-0\"><span class=\"badge bg-light card-link\"># "+.[2]+"</span><span class=\"badge bg-light\"># "+.[5]+"</span></div></div></li>"' \
	| jq -c '.|{item:.}' \
	| jq -c '.' > data/links.json
```

```sh
cp data/index.json data/links.json public/data
```

# Pretty print some of the ads

```sh
gunzip -c data/ads_enriched.json.gz | jq -c '.' | head -n 5 | jq '.'
```
