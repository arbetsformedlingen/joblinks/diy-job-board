
/**
const channel4Broadcast = new BroadcastChannel('channel4');
window.postMessage({key: "test"});
window.onmessage = (event) => {
  var value = event.data.key;
  console.log(value);
  load_links();
  load_index();
}
*/

async function load_index2(){
  fetch('data/index.json', {
    method: 'GET'
  }).then(response => response.json()).then(function(data) {
    idx = lunr.Index.load(data);
    $.event.trigger({
      type: "ready",
      message: "Ready to search!",
      time: new Date()
    });
      console.log("Laddat index");
    }).catch(function(err) {
      console.log(err);
    });
}

async function load_index(){
  var xmlhttp = new XMLHttpRequest();
  var url = "data/index.json";

  xmlhttp.onload = function() { //readystatechange
    if (this.readyState == 4 && this.status == 200) {
      var myArr = this.responseText;
      idx = lunr.Index.load(JSON.parse(myArr));
      $.event.trigger({
        type: "ready",
        message: "Ready to search!",
        time: new Date()
      });
    }
  };

  xmlhttp.open("GET", url, true);
  xmlhttp.send(null);
}
//load_index2();

async function load_links(){

  fetch('data/links2.json', {
    method: 'GET'
  })

  .then(response => response.json()).then(function(data) {
    ht = new Map();

    data.forEach(function(part) {
      try{
        ht.set(part.id,part.item);

      }catch(err){
        console.log(err);
      }});

      $.event.trigger({
        type: "ready",
        message: "Ready to search!",
        time: new Date()
      });

      console.log("Laddat fil i bakgrunden");
    }).catch(function(err) {
      console.log(err);
    });

  };
  //load_links();

  /**
  function load_links(){
    var xmlhttp = new XMLHttpRequest();
    var url = "data/links_test.json";

    xmlhttp.onload = function() { //readystatechange
      if (this.readyState == 4 && this.status == 200) {
        links = this.responseText;
        ht = new Map();
        parts = links.split('\n').slice(0,-1);
        parts.forEach(function(part) {
          try{
            var data = JSON.parse(part);
            ht.set(data.id, data.item);
          }catch(err){
            console.log(err);
          }
        });
        $.event.trigger({
          type: "ready",
          message: "Ready to search!",
          time: new Date()
        });
      }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
  }
  */
