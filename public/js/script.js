//console.clear();

var trace = console.log.bind(console);
var listSelector = "#question-list-container li";

var loaded = 1;
var queueSearchTerm=null;

const channel4Broadcast = new BroadcastChannel('channel4');
channel4Broadcast.onmessage = (event) => {
  console.log(event.message);
    loaded = loaded+1;
  queueSearchTerm=sessionStorage.getItem('queue');
  if(queueSearchTerm!==null){
    console.log("Köad sökterm: "+queueSearchTerm);
    search_query(queueSearchTerm);
  }

};

navigator.serviceWorker.addEventListener('message', event => {
  console.log(event.data.msg, event.data.url);
});

var idx;
var links;
var container = document.getElementById('links');
var parts;
var ht;

function show(result){

  container.innerHTML = '';

  result.forEach(function(hit) {
    var html = ht.get(hit.ref);
    container.insertAdjacentHTML('beforeend', html);
  });

}

function search_query(searchTerm) {
  var uri = "search?"+searchTerm;
  fetch(uri, {
    method: 'GET'
  })
  .then(response => response.json()).then(function(data) {
    container.innerHTML = '';
    //$('#antal').text(data.length);
    document.getElementById("antal").value = data.length;
    data.forEach(function(part) {
      try{
        container.insertAdjacentHTML('beforeend', part.link);
      }catch(err){
        console.log(err);
      }});
      console.log("Visat sökresultat");
    }).catch(function(err) {
      console.log(err);
    });
    sessionStorage.setItem('searchCompleted', true);
    sessionStorage.removeItem('queue');
}

function search(searchTerm) {
  var results = idx.search("title:"+searchTerm);
  //$(listSelector).removeClass("show");
  //var container = document.getElementById('links');
  $('#antal').text(results.length);
  console.log("Antal träffar: "+results.length);
  show(results);

  sessionStorage.setItem('searchCompleted', true);
  sessionStorage.removeItem('queue');
}

if(sessionStorage.getItem('queue')!==null && sessionStorage.getItem('searchCompleted')!==true){
  $("#searchterm").val(sessionStorage.getItem('queue'));
}

document.addEventListener('afOnClick', (event) => {
  if(loaded<2){
    sessionStorage.setItem('queue', $(".digi-form-input__input").val());
    return;
  }
  console.time("search");
  search($(".digi-form-input__input").val());
  console.timeEnd("search");
  event.preventDefault();
})

document.getElementById("search2").addEventListener("click", function(event) {
  if(loaded===0){
    sessionStorage.setItem('queue', $("#searchterm").val());
    return;
  }
  console.time("search");
  //search($("#searchterm").val());
  search_query(document.getElementById("searchterm").value);
  console.timeEnd("search");
  event.preventDefault();
});

/**
$("#search2").click(function(event) {
  if(loaded===0){
    sessionStorage.setItem('queue', $("#searchterm").val());
    return;
  }
  console.time("search");
  //search($("#searchterm").val());
  search_query($("#searchterm").val());
  console.timeEnd("search");
  event.preventDefault();
})
*/
