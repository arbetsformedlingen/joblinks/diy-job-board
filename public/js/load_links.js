

function streamJSON(url, callback) {
  return new Promise(function(resolve, reject) {

    var xhr = new XMLHttpRequest();
    var pos = 0;

    function processChunk(chunk) {
      try {
        var data = JSON.parse(chunk);
        count++;

      }
      catch (err) {
        reject(Error('Error parsing: ' + chunk));
        xhr.abort();
        return;
      }

      callback(data.item);
    }

    xhr.onprogress = function() {
     var parts = xhr.response.slice(pos).split('\n');

      parts.slice(0, -1).forEach(function(part) {
        processChunk(part);
        pos += part.length + 1;
      });
    };

    xhr.onload = function() {
      var chunk = xhr.response.slice(pos);
      if (chunk)
      processChunk(buf);


      resolve();
      $.event.trigger({
      type: "loadedHtml",
      message: "Hello World!",
      time: new Date()
     });
    };

    xhr.onloadend = function() {
       container.insertAdjacentHTML('beforeend', buf);

       $(document).ready(function(){
         $('a.card-title.text-primary').click(function() {
           $(this).attr('target','_blank');
         });
       });
    };


    xhr.onerror = function() {
      reject(Error('Connection failed'));
    };

    xhr.responseType = 'text';
    xhr.open('GET', url, true);
    //xhr.setRequestHeader('Cache-Control', 'public');
    //xhr.setRequestHeaders('accept-encoding','gzip');
    //xhr.setRequestHeader('Content-type', 'application/json');
    xhr.send();
  });
}

  var container = document.getElementById('links');

  var buf = '';
  var i = 0;

  var count = 0;


  /*                streamJSON('data/links.json', function(comment) {
                    buf += comment;
                    if (i % 10000 === 0){

                    container.insertAdjacentHTML('beforeend', buf);

                    buf = '';
                    }
                    i++;
                  });
                  */
