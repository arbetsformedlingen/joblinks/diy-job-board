importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.4.1/workbox-sw.js');
importScripts('js/lunr-2.3.8.js');
//workbox.setConfig({modulePathPrefix: '/js/'});

workbox.precaching.precacheAndRoute([
  "data/index.json",
  "data/links.json"
]);


// Incrementing OFFLINE_VERSION will kick off the install event and force
// previously cached resources to be updated from the network.
const OFFLINE_VERSION = 1;
const CACHE_NAME = 'offline';
// Customize this with a different URL if needed.
const OFFLINE_URL = 'offline.html';

self.addEventListener('install', (event) => {
  event.waitUntil((async () => {
    const cache = await caches.open(CACHE_NAME);
    // Setting {cache: 'reload'} in the new request will ensure that the response
    // isn't fulfilled from the HTTP cache; i.e., it will be from the network.
    await cache.add(new Request(OFFLINE_URL, {cache: 'reload'}));
  })());
});

self.addEventListener('activate', (event) => {
  event.waitUntil((async () => {
    // Enable navigation preload if it's supported.
    // See https://developers.google.com/web/updates/2017/02/navigation-preload
    if ('navigationPreload' in self.registration) {
      await self.registration.navigationPreload.enable();
    }
  })());

  // Tell the active service worker to take control of the page immediately.
  self.clients.claim();
});

self.addEventListener('fetch', (event) => {

var url = decodeURI(event.request.url);

if(url.includes("search")){
  var searchResult = search(url.split("?")[1]);
  var test = show(searchResult);

  const ndJson = test.map(JSON.stringify).join('\n');
  var clean = ndJson.replace(/^"|"$/g, '');
  event.respondWith(
    new Response(clean,
      {
        headers: { 'Content-Type': 'application/json' } //application/x-ndjson
      })
    );
  }




  // We only want to call event.respondWith() if this is a navigation request
  // for an HTML page.
  if (event.request.mode === 'navigate') {
    event.respondWith((async () => {
      try {
        // First, try to use the navigation preload response if it's supported.
        const preloadResponse = await event.preloadResponse;
        if (preloadResponse) {
          return preloadResponse;
        }

        const networkResponse = await fetch(event.request);
        return networkResponse;
      } catch (error) {
        // catch is only triggered if an exception is thrown, which is likely
        // due to a network error.
        // If fetch() returns a valid HTTP response with a response code in
        // the 4xx or 5xx range, the catch() will NOT be called.
        console.log('Fetch failed; returning offline page instead.', error);

        const cache = await caches.open(CACHE_NAME);
        const cachedResponse = await cache.match(OFFLINE_URL);
        return cachedResponse;
      }
    })());
  }
  });

  // If our if() condition is false, then this fetch handler won't intercept the
  // request. If there are any other fetch handlers registered, they will get a
  // chance to call event.respondWith(). If no fetch handlers call
  // event.respondWith(), the request will be handled by the browser as if there
  // were no service worker involvement.

var idx;
var ht;
async function load_index2(){
  fetch('data/index.json', {
    method: 'GET'
  }).then(response => response.json()).then(function(data) {
    idx = lunr.Index.load(data);
      console.log("Laddat index");
    }).catch(function(err) {
      console.log(err);
    });
}

load_index2();

async function load_links(){

  fetch('data/links.json', {
    method: 'GET'
  })

  .then(response => response.json()).then(function(data) {
    ht = new Map();

    data.forEach(function(part) {
      try{
        ht.set(part.id,part.item);

      }catch(err){
        console.log(err);
      }});
      //channel4Broadcast.postMessage({key: "ready"});
      console.log("Laddat annonser");
    }).catch(function(err) {
      console.log(err);
    });

  };
  load_links();

  function search(searchTerm) {
    var results = idx.search("title:"+searchTerm);
    console.log("Antal träffar: "+results.length);
    return results;
  }

  function show(result){
    var response = new Array();
    result.forEach(function(hit) {
      var html = ht.get(hit.ref);
      var ad = new Object();
      ad.link = html;
      response.push(ad);
    });
    return response;
  }


// Cache CSS, JS, and Web Worker requests with a Stale While Revalidate strategy

workbox.routing.registerRoute(

  /\.(?:css|js)$/,
  // Use a Stale While Revalidate caching strategy
  new workbox.strategies.StaleWhileRevalidate({
    // Put all cached files in a cache named 'assets'
    cacheName: 'assets',
    plugins: [
      // Ensure that only requests that result in a 200 status are cached
      new workbox.cacheableResponse.Plugin({
        statuses: [200],
      }),
    ],
  }),
);

// Cache images with a Cache First strategy
workbox.routing.registerRoute(
  // Check to see if the request's destination is style for an image
  /\.(?:json)$/,
  // Use a Cache First caching strategy
  new workbox.strategies.CacheFirst({
    // Put all cached files in a cache named 'images'
    cacheName: 'data',
    plugins: [
      // Ensure that only requests that result in a 200 status are cached
      new workbox.cacheableResponse.Plugin({
        statuses: [200],
      }),
      // Don't cache more than 50 items, and expire them after 1 days
      new workbox.expiration.Plugin({
        maxEntries: 10,
        maxAgeSeconds: 60 * 60 * 12, // 30 Days
      }),
    ],
  }),
);

workbox.routing.registerRoute(
  ({url}) => url.origin === 'https://fonts.gstatic.com',
  new workbox.strategies.CacheFirst({
    cacheName: 'fonts',
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
        maxEntries: 30,
      }),
    ],
  })
);

workbox.routing.registerRoute(
  new RegExp('/.*'),
  new workbox.strategies.NetworkFirst()
);
